# Bash

## Path manipulation

 - get absolute path of a file: `file_dir="$(cd "$(dirname "$0")" && pwd)"`
 - get parent: `parent_dir="$(dirname "${file_dir}")"`


## Loop in numeric range

```bash
#!/bin/bash
# Tested using bash version 4.1.5
for ((i=1;i<=100;i++)); 
do 
   # your-unix-command-here
   echo $i
done
```
