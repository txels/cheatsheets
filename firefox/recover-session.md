# Recover a Firefox session lost after an update

After an upgrade of Firefox, my previous open tabs and windows were lost.

Steps to recovery:

 - Browse to `about:profiles`. Find out the root folder to the active profile.
 - Within that folder, `sessionstore-backups` contains a number of files. In my case, `previous.jsonlz4` was the one I wanted (it was the largest by far - many open tabs and windows).
 - Close Firefox. Copy the file to replace the current one (at the root of the profile folder, named `sessionstore.jsonlz4`
 - Restart Firefox

Concrete commands used:

    $ cd ~/.mozilla/firefox
    $ cp u1ma5ekc.default-release/sessionstore-backups/previous.jsonlz4 u1ma5ekc.default-release/sessionstore.jsonlz4

...where `u1ma5ekc.default-release` is my active profile folder found in `about:profiles`.
