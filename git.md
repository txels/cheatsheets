# Git

## Use a global .gitignore

Edit ~/.gitignore, then set it up with:

    git config --global core.excludesfile ~/.gitignore


# Rebase onto: change the base branch for your current branch

Some cases:

- you started a bugfix out of develop, but it should be applied to a release branch
- you started on a branch and want to rebase it on some colleague's changes

## Command

```
git rebase --onto <new base> <old base> <branch you're moving>
```

## Example

```
git rebase --onto release-2.7 develop develop-mychanges
```


http://www.git-scm.com/book/en/v2/Git-Branching-Rebasing#More-Interesting-Rebases

Full example:
- I created a branch release-2.11-something
- This should have been created on release 2.10 as `release-2.10-something`

Assuming you are on branch release-2.11-something:

```
git fetch  # to make sure you have an updated release-2.10
git checkout -b release-2.10-something  # my new branch name
git rebase --onto release-2.10 release-2.11 release-2.10-something
```

This will take all commits in release-2.10-something (which initally is 
the same as release-2.11-something) that are not in release-2.11 and 
"transfer them" on top of release-2.10 HEAD. After this, the branch
release-2.10-something will have changed and will be based on release-2.10.
The initial branch release-2.11-something will stay as it was.
The branch release-2.10-something will not include undesired commits from release-2.11.


```
What we had:

               o---o---o---o---o  release-2.10
                    \
                     o---o---o---o---o  release-2.11
                                      \
                                       o---o---o  my-work

We want our tree to look like this:

               o---o---o---o---o  release-2.10
                   |            \
                   |             o'--o'--o'  my-work
                    \
                     o---o---o---o---o  release-2.11
```


