# Gnome

## Keyboard bindings and configuration

### dconf

```sh
# Dump configuration for a "path":
dconf dump /org/gnome/
# Edit configuration live:
dconf-editor
```

### gsettings

Use to set keybindings

```sh
#  (mutter is the default window manager for Ubuntu)
# Tile window left or right
gsettings set org.gnome.mutter.keybindings toggle-tiled-left "['<Control><Shift>Left']"
gsettings set org.gnome.mutter.keybindings toggle-tiled-right "['<Control><Shift>Right']"

# General window manager keybindings:
# maximise or center windows
gsettings set org.gnome.desktop.wm.keybindings move-to-center "['<Control><Shift>c']"
gsettings set org.gnome.desktop.wm.keybindings toggle-maximized "['<Control><Shift>End']"

```
