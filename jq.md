# JQ

## Select fields in list of objects matching condition.

Return as separate objects (like concatenated mini-jsons that don't make a valid json):

    jq '.[]|select(.up_to_date==false)'

Keep the data as an array (valid JSON):

    jq 'map(select(.up_to_date==false))'

Multiple conditions, including regex match:

    jq 'map(select(.up_to_date==false and (.current.since|test("months|years")) ))'

## Select a subset of fields

    jq 'map({name, email, role, expired})'

Nested fields, renamed:

    jq 'map({name:.metadata.name,status:.status.phase})'

## Get keys for an object

    jq 'keys' file.json

For a list of objects, get a flat enumeration of keys:

    jq -r '.[0] | keys | .[]'
