# Linux package management (Debian/Ubuntu)

Find the package where a binary comes from:

    dpkg -S /path/to/file
    dpkg -S $(which binary)

List files for a package:

    dpkg -L <package>

