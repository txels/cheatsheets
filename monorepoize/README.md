# Monorepo

## Ensure Heroku apps understand monorepo

For created apps:

    APP=fifty-skills-pipeline-staging 
    heroku buildpacks:add -a ${APP} https://github.com/50skills/heroku-buildpack-monorepo -i 1
    heroku config:set -a ${APP} APP_BASE=server


For preview apps? app.json? Problem: must be at root of repo.
Can't have more than one "review app" associated with a repo.

    "buildpacks": [
      {
        "url": "https://github.com/50skills/heroku-buildpack-monorepo"
      },
      {
        "url": "heroku/python"
      }
    ],
    "env": {
      "APP_BASE": "server"
    }


Useful heroku cmds:

    # Get env vars in "shell" format (use -j instead of s for JSON) 
    heroku config -a fifty-skills-pipeline-staging -s

Create review app using API (not available on CLI):

    PR=1
    BRANCH=deploy
    PIPELINE=44a5b4b1-4a43-4699-9eb5-d41b4698168b
    SHA=$(git rev-parse HEAD)
    http --auth carles@barrobes.com:${HEROKU_API_KEY} \
      https://api.heroku.com/review-apps \
      pipeline="${PIPELINE}" \
      branch="${BRANCH}" \
      pr_number:=${PR} \
      source_blob:='{"url": "https://github.com/50skills/monorepo/tarball/'${BRANCH}'", "version": "'${SHA}'"}' \
      Accept:"application/vnd.heroku+json; version=3"

https://github.com/50skills/monorepo/tarball/deploy



## Monorepo-ing tools

> You need to `pip install git-filter-repo` for these to work.

Create a bunch of shell functions:

    . ./monorepo.sh

Use them always from the parent directory where your repos live.

If you have some code on a working branch on an prior test monorepo
that you want to save as a patch for later:

```sh
# e.g. branch 'deploy'
monorepo_create_patch deploy
```

Assemble a fresh monorepo from the ui and server repos, and apply
the `deploy` patch created previously:

    monorepo_assemble

(this was hardcoded at the time of writing as this function was used to
recreate this monorepo from scratch while there was only one `deploy`
branch open)

Set remote to github and push all branches and tags:

    monorepo_fresh_push
