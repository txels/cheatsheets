#!/bin/bash

create_monorepo() {
    mv monorepo monorepo-$(date +"%Y%m%d-%H%M").bck
    git init monorepo
    pushd monorepo
    git commit -m "Initial commit" --allow-empty
    popd
} 


prepare_subrepo() {
    repo=$1
    rm -rf tmp/${repo}
    git clone git@github.com:50skills/${repo}.git tmp/${repo}
    pushd tmp/${repo}
    git-filter-repo --to-subdirectory-filter ${repo} --tag-rename :${repo}-
    popd
}

merge_subrepo() {
    repo=$1
    pushd monorepo
    git remote add ${repo} ../tmp/${repo}
    git fetch --all --tags
    git merge --no-edit ${repo}/master --allow-unrelated-histories
    git remote remove ${repo}
    popd
}

monorepo_assemble() {
    repos="server ui"

    pyenv shell 3.6.10
    create_monorepo
    for repo in ${repos}
    do
        prepare_subrepo ${repo}
        merge_subrepo ${repo}
    done
    monorepo_apply_patch deploy
}

monorepo_fresh_push() {
    pushd monorepo
    git remote add origin git@github.com:50skills/monorepo.git
    git push --all origin --force
    popd
}

monorepo_create_patch() {
    if [ "" == "$1" ]
    then
        echo "Please specify a branch..."
    else
        pushd monorepo
        branch=$1
        patchname="../monorepo-${branch}.patch"
        # back up previous patch
        mv ${patchname} ../monorepo-${branch}-$(date +"%Y%m%d-%H%M").patch || :
        git format-patch master..${branch} --stdout > ${patchname}
        echo "Patch created as ${patchname}"
        popd
    fi
}

monorepo_apply_patch() {
    if [ "" == "$1" ]
    then
        echo "Please specify a branch..."
    else
        pushd monorepo
        branch=$1
        git checkout -b ${branch}
        git apply -v ../monorepo-${branch}.patch
        git add .
        git commit -am "Monorepo ${branch}"
        popd
    fi
}
