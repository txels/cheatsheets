# Networking (Linux)

```sh
# List routing tables
$ netstat -rn

# Show devices on local network with mac addresses
$ arp

# Wake on LAN (e.g. for Synology NAS)
$ wakeonlan 90:09:d0:16:62:05
```
