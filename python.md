# Python

## Destructuring

Matching multiple values:

```python
head, *tail = [1, 2, 3, 4, 5]
head, *middle, tail = [1, 2, 3, 4, 5]
```
