# Error handling in Rust

Result is an Enum with two variants, `Ok` (with an OK value that makes sense for your program)
and `Err` (with an error value, typically implementing the `std::error::Error` trait,
e.g. `std::io::Error`, but this is not required):

```rust
enum Result<T, E> {
    Ok(T),
    Err(E),
}
```

Usage:
```rust
    let data = match operation_result {
        Ok(data) => data,
        Err(error) => ...,
    }
```

Alternatives to `match`:

 - `.unwrap()`, `.expect(message)` - Get OK value or panic [with message] - PRO tip: prefer `expect`
 - Closure: `result.unwrap_or_else(|error| => {})`

Error propagation using `?`:

 - If `Ok(value)`, extract `value`, if `Err(error)` then `return error` from containing function
 - will use the `from` function of the `From` trait to convert error values

The `?` operator can also be used on `Option` values, to *early-return* a `None`. But you can't mix both (i.e. convert Option <-> Result with ?). But you can convert explicitly `result.ok()`, `option.ok_or(error_value)`.

## Custom errors with `thiserror`

The `thiserror` crate allows you to easily implement the `Error` trait on your custom errors.

Example:

```rust
use thiserror::Error;

#[derive(Error, Debug)]
pub enum OurspaceError {
}
```

## Additional resources
 
Crates that help with error handling:

 - https://crates.io/crates/anyhow - idiomatic error handling: automatically convert any error values to `anyhow::Error`
 - https://crates.io/crates/thiserror - convenient derive macro for std's Error trait

Learning resources:

 - https://www.lpalmieri.com/posts/error-handling-rust/
