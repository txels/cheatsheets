# Setting up or Upgrading OSX

Install xcode CLI tools:

    xcode-select --install

If you upgraded your mac's OS after installing them, you will need to manually
upgrade them via "Software Update".
